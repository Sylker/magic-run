
2018, Sylker Teles

This is a README file for the Magic Run Project

1. Intro

This project is a Unity 3D test developed in accordance with the PDF document UnityTest 3.0 found in this repository. 
I tried to achieve as many tiers requirements as possible, although some wouldn't fit the game style or I didn't have the time to go for it. This document summarises some project aspects.
By the other hand, some features not present as requirement on any tier were implemented, as sound effects and background music using Unity's AudioMixer and Snapshots to achieve a smooth transition between sound levels.

2. Achieved requirements.

Tier 0:

- Although there's no menu, user can interact with the hud by tapping to start or reload the game, using butt to pause, resume, mute and unmute game sound.

- The Tier 0 Subway Surfers was used as reference for the game mechanics. 

Tier 1:

- The game runs on 3D and 2D modes. On 2D mode, it uses some sprites as assets for the the coins and enemies.

- The player collides with enemies, obstacles and coins;

- There are particle effects for the smoke chasing the player;

- Enemies, obstacles and coins are spawned in real time.

- There's Normal map shaders on walls and floor, a self illuminated pattern on a old Jug used as obstacle and transparent shader on windows.

Tier 2:

- Camera moves changing its target in the game beginning. Also, it changes position between the 3D and 2D side scrolling modes.

Tier 3:

- The smoke chasing the player uses a custom shader to render it on top of everything.

3. External Resources

- Coin sound FX by NenadSimic from OpenGameArt.org
- Bat sprite sheet by Bagzie from OpenGameArt.org
- Toon Smoke VFX by Luke Peed from Unity Asset Store
- Most of assets are from my game Matemagos, including the main character, enemies, obstacles and the background music.
- The castle and the coins (3D and 2D) were created specially for this project.
- All codes were written from scratch and are commented as much as possible. Some like the  particle over objects shaders required some research.

4. Known issues
- Sometime, the character will go through the last wall on the right into the void.
- Sound preferences not being saved properly.

Enjoy!