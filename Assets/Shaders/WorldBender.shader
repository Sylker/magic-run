﻿// based on Animal Crossing Curved World Shader by Alastair Aitchison
// combined with Unity's built in Mobile-Bumped shader

Shader "Custom/WorldBender" 
{
	Properties 
    {

    // diffuse texture
    _MainTex ("Base (RGB)", 2D) = "white" {}
    [NoScaleOffset] _BumpMap ("Normalmap", 2D) = "bump" {}

    // bending degree
    _Curvature ("Curvature", Float) = 0.001
}

    SubShader 
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Lambert vertex:vert addshadow noforwardadd

        // shaderlab properties
        uniform sampler2D _MainTex;
        sampler2D _BumpMap;
        uniform float _Curvature;

        // we only need a single set of UV texture mapping coordinates
        struct Input  { float2 uv_MainTex; };

        // apply the curvature
        void vert( inout appdata_full v)
        {
            // change vertex coordinates to world space
            float4 vv = mul( unity_ObjectToWorld, v.vertex );

            // and adjust the coordinates to be relative to the camera position
            vv.xyz -= _WorldSpaceCameraPos.xyz;

            // lower the height of vertex in y axis relative to camera position
            vv = float4( 0.0f, (vv.z * vv.z) * - _Curvature, 0.0f, 0.0f );

            // now put back vertex coordinates in model space
            v.vertex += mul(unity_WorldToObject, vv);
        }

        // the default surface shader
        void surf (Input IN, inout SurfaceOutput o) 
        {
            half4 c = tex2D (_MainTex, IN.uv_MainTex);
            o.Albedo = c.rgb;
            o.Alpha = c.a;
            o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
        }
        ENDCG
    }

    // FallBack "Mobile/Diffuse"
}
