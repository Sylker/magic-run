﻿// Written by Sylker Teles, 2018
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Mute and Unmute the game.
/// </summary>
public class Mute : MonoBehaviour 
{
    // the Button UI
    public Button muteButton;

    // button imagem to change its color later
    public Image buttonImage;

    // button icon image to change the button icon later
    public Image buttonIcon;

    // pause icon sprite
    public Sprite muteIcon;

    // Use this for initialization
    public Sprite playIcon;

    // mute property to other classes
    public static bool mute;

	// Use this for initialization
	void Start () 
    {
        // add a funcion to the button
        muteButton.onClick.AddListener(MuteAudio);

        // loads player saved mute state
        if (PlayerPrefs.HasKey("Mute"))
        {
            int muteValue = PlayerPrefs.GetInt("Mute");

            // if the user doesn't like music, mute the game
            if (muteValue == 1) MuteAudio();

            // and playe the music nevertheless xD
            Interface.instance.audio.music.Play();
        }
    }
	
    /// <summary>
    /// Mutes the audio.
    /// </summary>
    void MuteAudio()
    {
        // no sound, play it
        if (mute)
        {
            // change button icon
            buttonIcon.sprite = muteIcon;

            // change button color
            buttonImage.color = Color.red;

            // fade the audio
            Interface.instance.audio.Unmute(.1f);

            // saves the player prefs
            PlayerPrefs.SetInt("Mute", 0);

            // update the static variable
            mute = false;
        }

        // sound is playing, stop it
        else
        {
            // fade the audio
            Interface.instance.audio.Mute(.1f);

            // change button icon
            buttonIcon.sprite = playIcon;

            // change button color
            buttonImage.color = Color.green;

            // saves the player prefs
            PlayerPrefs.SetInt("Mute", 1);

            // update the static variable
            mute = true;
        }
    }
}
