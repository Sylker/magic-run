﻿// Written by Sylker Teles, 2018

using System.Collections;
using UnityEngine;

/// <summary>
/// Controls the player and its behaviour.
/// </summary>
public class Player : MonoBehaviour 
{
    // allow us to wait before the game begins, to show a tutorial, for instance
    public static bool ready = false;

    // same waiting time for 2D mode
    public static bool readySide = false;

    // whether we are in a 2D or 3D modes
    public static bool sideScrolling;

    // animator controller for the player and character
    public Animator playerAnimator, characterAnimator;

    // speed multiply
    public float speed;

    // vehicle
    public Broom broom;

    // let's add an OnTriggerEnter Action to the vehicle
    // so we can track collisions
    private void OnEnable()
    {
        broom.onTriggerEnter += TriggerEnter;
    }

    // and remove it after the player disabling
    private void OnDisable()
    {
        broom.onTriggerEnter -= TriggerEnter;

        // also reset control variables
        sideScrolling = false;
        ready = false;
    }

    void Update () 
    {
        // store player's current position
        Vector3 newPosition = transform.position;

        // in 2D mode, move it over x axis
        if (sideScrolling) newPosition.x += Time.deltaTime * speed;

        // otherwise, move it towards z
        else newPosition.z += Time.deltaTime * speed;

        // update player's position with the new values
        transform.position = newPosition;
	}

    // when getting inside triggers
    void TriggerEnter (Collider obj)
    {
        // on each module, create a new one ahead
        if (obj.name.Contains("Corridor"))
            FindObjectOfType<Replicator>().CreateModule();

        // on corners, swap between 2D and 3D modes
        if (obj.name.Contains("Corner")) ChangeDirection(obj);

        // we hit an enemy or obstacle
        if (obj.tag == "Enemy")
        {
            // depending on direction, plays a different animation
            if (sideScrolling) playerAnimator.SetTrigger("hit_side");
            else playerAnimator.SetTrigger("hit_front");

            // update smoke's position, it gets closer when player gets hit
            FindObjectOfType<Smoke>().Hit();

            // animate the character
            characterAnimator.SetTrigger("damage");

            // reduce speed for a little bit
            speed = speed / 3;

            // recover the speed over time
            StartCoroutine(SpeedRecovery());
        }

        // we hit a coin
        else if (obj.tag == "Coin")
        {
            // destroy the coin object
            Destroy(obj.gameObject);

            // add one coin to player's savings
            FindObjectOfType<Interface>().AddCoin();
        }
    }

    void ChangeDirection (Collider obj)
    {
        // first time on 2D mode
        if (!readySide)
        {
            // reset the ready state, so enemies won't come
            ready = false;

            // opens the 2D tutorial
            Interface.instance.dialogs.Open(2, delegate
            {
                // this will execute after the tutorial
                ready = true; // player will be ready again
                readySide = true; // including for 2D

                // and the HUD will show up again
                Interface.instance.FadeCanvas(Interface.instance.HUD);
            });

            // Hide HUD for a while
            Interface.instance.FadeCanvas(Interface.instance.HUD);

            // mark tutorials as read, so it won't show up next time
            Interface.instance.tutorials = true;

            // Get the player ready even if user don't click anywhere
            Interface.instance.GetReady();
        }

        // normalize player's position after a turn
        Vector3 newPosition = transform.localPosition;

        // in side scrolling mode
        if (sideScrolling) newPosition.x = obj.transform.position.x;

        // or in normal mode
        else newPosition.z = obj.transform.position.z;

        // update player's position
        transform.localPosition = newPosition;

        // update the sideScrolling variable
        sideScrolling = !sideScrolling;

        // update smoke distance
        FindObjectOfType<Smoke>().UpdateDistance();
    }

    // recovers player's speed over time
    IEnumerator SpeedRecovery ()
    {
        // this is the regular speed
        float regularSpeed = speed * 3;

        // accelerate smoothly
        while (speed < regularSpeed)
        {
            speed += .5f;
            yield return new WaitForEndOfFrame();
        }
    }
}