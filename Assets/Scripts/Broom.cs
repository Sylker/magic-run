﻿// Written by Sylker Teles, 2018
using System;
using UnityEngine;

public class Broom : MonoBehaviour
{
    // animator controller for the vehicle
    public Animator controller;

    // animator controller for the camera
    public Animator cameraController;

    // user is swiping on the screen?
    bool swiping, swipped;

    // store last touch positio on screen
    Vector2 lastTouch;

    // actions for the colliders
    public event Action<Collider> onTriggerEnter;

    private void OnTriggerEnter(Collider other)
    {
        if (onTriggerEnter != null) onTriggerEnter(other);

        // on corners, swap between 2D and 3D modes
        if (other.name.Contains("Corner"))
        {
            controller.SetFloat("y_axis", -1); // reset the y axis
            controller.SetFloat("x_axis", 0); // reset the x axis

            // update the animator
            cameraController.SetBool("sidescrolling", Player.sideScrolling);
            controller.SetBool("sidescrolling", Player.sideScrolling);
        }
    }

    private void Update()
    {
        // input controller
        if (controller)
        {
            // get axis values from animator controller
            float x_axis = controller.GetFloat("x_axis");
            float y_axis = controller.GetFloat("y_axis");

            // Movement controls

            // arrow keys controller (on PC)
            #if UNITY_EDITOR
            if (Input.GetKeyUp(KeyCode.RightArrow)) x_axis += 1;
            if (Input.GetKeyUp(KeyCode.LeftArrow)) x_axis -= 1;
            if (Input.GetKeyUp(KeyCode.UpArrow)) y_axis += 1;
            if (Input.GetKeyUp(KeyCode.DownArrow)) y_axis -= 1;

            // gestures controller (on mobile)
            #else
            if (Input.touchCount == 0) return;

            // swiping is happening
            if (Input.GetTouch(0).deltaPosition.sqrMagnitude != 0)
            {
                // first touch
                if (swiping == false)
                {
                    // mark swiping as true for the next update
                    swiping = true;

                    // store the last position for later use
                    lastTouch = Input.GetTouch(0).position;

                    // skip to the next update
                    return;
                }

                // swiping continues
                else
                {
                    // 
                    if (!swipped)
                    {
                        // get the direction by the difference between current
                        // and last positions:
                        Vector2 direction = 
                            Input.GetTouch(0).position - lastTouch;

                        // horizontal
                        if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
                        {
                            if (direction.x > 0) x_axis += 1;  // swipe right
                            else x_axis -= 1; // swipe left
                        }

                        // vertical
                        else
                        {
                            if (direction.y > 0) y_axis += 1; // swipe up
                            else y_axis -= 1; // swipe down
                        }
                        swipped = true;
                    }
                }
            }

            // no touching, reset variables
            else
            {
                swiping = false;
                swipped = false;
            }
            #endif

            // 2D mode
            if (Player.sideScrolling)
            {
                x_axis = 0;
                y_axis = Mathf.Clamp(y_axis, -1, 1); // clamp y between -1 and 1
                controller.SetFloat("y_axis", y_axis); //send value to animator
            }

            // 3D mode
            else
            {
                y_axis = -1; // reset the y axis
                x_axis = Mathf.Clamp(x_axis, -1, 1); // clamp x between -1 and 1
                controller.SetFloat("x_axis", x_axis); //send value to animator
            }
        }
    }
}
