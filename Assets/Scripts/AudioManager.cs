﻿// Written by Sylker Teles, 2018
using UnityEngine;
using UnityEngine.Audio;

/// <summary>
/// Audio Manager. Controls audio volume and transition for the game.
/// </summary>
public class AudioManager : MonoBehaviour 
{
    // game's music
    public AudioSource music;

    // coin sound fx
    public AudioSource coinSound;

    // normal volume
    public AudioMixerSnapshot normal;

    // mute
    public AudioMixerSnapshot mute;

    /// <summary>
    /// Mute in the specified time.
    /// </summary>
    /// <returns>The mute.</returns>
    /// <param name="time">Time.</param>
	public void Mute (float time = 1) 
    {
        mute.TransitionTo(time);
	}
	
    /// <summary>
    /// Unmute in the specified time.
    /// </summary>
    /// <returns>The unmute.</returns>
    /// <param name="time">Time.</param>
    public void Unmute (float time = 1) 
    {
        normal.TransitionTo(time);
	}
}
