﻿// Written by Sylker Teles, 2018
using UnityEngine;

/// <summary>
/// Keep the size of Y of Transform target the same of another Transform.
/// It can be used to keep background size same as text amount for instance.
/// </summary>
[ExecuteInEditMode]
public class AutoSizeUI : MonoBehaviour 
{
    public RectTransform reference; // the object to copy the size
    public RectTransform target; // the object to transfer the size
    public float offset = 1; // in case of an extra size needed

	void Update () 
    {
        // no objects, let's skip this
        if (!target || !reference) return;

        // get current size
        Vector2 newSize = target.sizeDelta;

        // change Y size and add the offset value
        newSize.y = reference.sizeDelta.y+offset;

        // update the target size
        target.sizeDelta = newSize;
	}
}
