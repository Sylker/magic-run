﻿// Written by Sylker Teles, 2018

using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Pauses and Resumes the Game.
/// </summary>
public class Pause : MonoBehaviour 
{
    // the Button UI
    public Button pauseButton;

    // button imagem to change its color later
    public Image buttonImage;

    // button icon image to change the button icon later
    public Image buttonIcon;

    // pause icon sprite
    public Sprite pauseIcon;

    // resume icon sprite
    public Sprite playIcon;

    // Use this for initialization
	void Start () 
    {
        // add a funcion to the button
        pauseButton.onClick.AddListener(PauseGame);
	}

    void PauseGame ()
    {
        // game is paused, resume it
        if (Time.timeScale < .1f) 
        {
            // change button icon
            buttonIcon.sprite = pauseIcon;

            // change button color
            buttonImage.color = Color.red;

            // freeze the game
            Time.timeScale = 1;

            // fade the audio
            if (!Mute.mute)
                Interface.instance.audio.Unmute(.1f);
        }

        // not paused, stop it
        else 
        { 
            // fade the audio
            if (!Mute.mute)
                Interface.instance.audio.Mute(.1f);

            // change button icon
            buttonIcon.sprite = playIcon;

            // change button color
            buttonImage.color = Color.green;

            // unfreeze the game
            Time.timeScale = 0;
        }
    }
}
