﻿// Written by Sylker Teles, 2018
using UnityEngine;
using System.Collections;

/// <summary>
/// This is the main enemy chasing the player.
/// When the smoke reaches the player, game is over.
/// </summary>
public class Smoke : MonoBehaviour 
{
    // there are different smokes for 2D and 3D modes
    public Transform backSmoke, sideSmoke;

    // distance and max distance from Player
    float distance, maxDistance;

    // wait until the game is started by the player
    IEnumerator Start () 
    {
        // how far from player smoke can be
        maxDistance = -4;

        // and it is the starting distance
        distance = maxDistance;

        // wait until the user starts the game
        while (!Interface.instance.started) yield return null;

        // uptade the distance
        UpdateDistance();
	}
	
	void Update () 
    {
        // game isn't started yet, return
        if (!Interface.instance.started)  return;

        // game isn't over and the smoke reaches the player
        if (distance >= -1 && !Interface.instance.gameOver)
        {
            // this is the end...
            FindObjectOfType<Interface>().GameOver(); 
        }

        // activate the correct smoke accoring to the direction
        backSmoke.gameObject.SetActive(!Player.sideScrolling);
        sideSmoke.gameObject.SetActive(Player.sideScrolling);

        // if the smoke is close, put it back slowly
        if (distance > maxDistance)
            distance -= .1f * Time.deltaTime;
	}

    // when player gets hit, smoke will get closer
    public void Hit ()
    {
        distance += .5f;
        UpdateDistance();
    }

    // this will update the distance between the smoke and the player
    public void UpdateDistance ()
    {
        // store the current 3D local position
        Vector3 newPos = backSmoke.localPosition;

        // update the z axis
        newPos.z = distance;

        // uptate 3D smoke's position
        backSmoke.localPosition = newPos;

        // store the current 2D local position
        newPos = sideSmoke.localPosition;

        // update the x axis
        newPos.x = distance;

        // uptate 2D smoke's position
        sideSmoke.localPosition = newPos;
    }
}