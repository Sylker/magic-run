﻿// Written by Sylker Teles, 2018
using UnityEngine;

/// <summary>
/// Makes enemies look at player all the time
/// </summary>
public class LookAtPlayer : MonoBehaviour 
{
    // the player's Transform
    Transform target;

	void OnEnable () 
    {
        // get the target on object enabling
        target = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	void LateUpdate () 
    {
        // no target, skip it
        if (!target) return;

        // or look at player
        transform.LookAt(target);
		
	}
}
