﻿// Written by Sylker Teles, 2018
using UnityEngine;

/// <summary>
/// Controls coins rotation over time.
/// </summary>
public class Coin : MonoBehaviour 
{
    // the rotation speed
    public float rotationSpeed;

	// Update is called once per frame
	void Update () 
    {
        // keeps turning around
        transform.Rotate(Vector3.forward * (rotationSpeed * Time.deltaTime));
	}
}
