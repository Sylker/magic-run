﻿// Written by Sylker Teles, 2018
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Dialog box. Just a way to get to thsese variables faster.
/// </summary>
public class DialogBox : MonoBehaviour 
{
    // the title text UI
    public Text title;

    // the content text UI
    public Text content;
}
