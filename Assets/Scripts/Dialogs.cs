﻿// Written by Sylker Teles, 2018
using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Shows and Hides dialogs boxes.
/// </summary>
public class Dialogs : MonoBehaviour 
{
    // the dialog box
    public DialogBox dialogBox;

    // a set of dialogs
    public Dialog[] dialogs;

    // when dialog closes, run this action
    public event Action OnDialogClose;

    // current dialog index
    int current;

    // dialog is open
    bool open;

    // current dialog GameObject
    GameObject currentCanvas;

    /// <summary>
    /// Open a dialog box with dialog index i and executes a given action
    /// </summary>
    /// <returns>The open.</returns>
    /// <param name="i">The index.</param>
    /// <param name="action">Action.</param>
    public void Open (int i, Action action = null)
    {
        // get rid of old actions
        OnDialogClose = null;

        // if theres an action to execute
        if (action != null)

            // put it on hold
            OnDialogClose += action;

        // instantiate the dialog box
        GameObject newCanvas = Instantiate(dialogBox.gameObject, transform);

        // and get its DialogBox class
        DialogBox newBox = newCanvas.GetComponent<DialogBox>();

        // uptade its title and content
        newBox.title.text = dialogs[i].title;
        newBox.content.text = dialogs[i].content;

        // update the current canvas
        currentCanvas = newCanvas;

        // shows the new dialog box
        Interface.instance.FadeCanvas(newCanvas);

        // wait a little before allows sthe player to close it
        StartCoroutine(ReadingTime());
    }

    private void Update()
    {
        // no box open, return
        if (!open) return;

        // otherwise, close it
        if (Input.anyKey) Close();
    }

    /// <summary>
    /// Close a dialog box.
    /// </summary>
    public void Close ()
    {
        // reset the open state
        open = false;

        // fade the dialog box away
        Interface.instance.FadeCanvas(currentCanvas);

        // if there's an action to execute, run it
        if (OnDialogClose != null) OnDialogClose();

        // otherwise, show the HUD back
        else Interface.instance.FadeCanvas(Interface.instance.HUD);

        // and reset the actions list
        OnDialogClose = null;
    }

    // wait for a while before allows the player to close the dialog
    IEnumerator ReadingTime ()
    {
        yield return new WaitForSeconds(1);
        open = true;
    }
}

/// <summary>
/// Dialog Class.
/// </summary>
[Serializable]
public class Dialog
{
    // Dialog title
    [SerializeField] public string title;

    // Dialog content
    [SerializeField, TextArea] public string content;
}