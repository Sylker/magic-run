﻿// Written by Sylker Teles, 2018

using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Controls the Game User Interface and important game events
/// like GameStart and GameOver.
/// </summary>
public class Interface : MonoBehaviour 
{
    // let's keep this class between scenes by storing in in a static instance
    public static Interface instance;

    // started will be true when the player tap the screen for the 1st time
    // until then show the main screen
    [HideInInspector] public bool started;

    // when game is over, mark it for other classes 
    [HideInInspector] public bool gameOver;

    // when pleayer reads all tutorials, mark this as true we the dialogs won't 
    // show up next time
    [HideInInspector] public bool tutorials;

    // a set of canvases to show and hide accordingly
    public CanvasGroup mainLogo, startText, endScreen, HUD, recordText;

    // audio controller class
    new public AudioManager audio;

    // dialogs controller class
    public Dialogs dialogs;

    // UI texts to update
    public Text coinsTxt, coinsRecordTxt;

    // top score for coins
    int coinsRecord;

    // player's total coins
    public int Coins { get; private set; }

    // a blinking effect for UI
    Coroutine blinking;

    // waint a little before reload the game
    bool readyToReload = false;

    private void Awake()
    {
        // no instance of Interface yet, make this one an instance
        if (instance == null) instance = this;

        // otherwise destroy this instance
        else Destroy(this.gameObject);

        // keep this instance alive between scenes
        DontDestroyOnLoad(this);
    }

    // Use this for initialization
    void Start ()
    {
        // start blinking the "Tap to Start" text
        blinking = StartCoroutine(BlinkCanvas(startText));

        // reset coins text UI
        coinsTxt.text = "0";

        // load saved coins record
        if (PlayerPrefs.HasKey("Coins")) 
            coinsRecord = PlayerPrefs.GetInt("Coins");
	}
	
	// Update is called once per frame
	void Update () 
    {
        // on tap
        if (Input.anyKey)
        {
            // starts the game
            if (!started && !gameOver) GameStart();

            // or reload if it is over
            else if (readyToReload) Reload();
        }
	}

    // let the game begins
    void GameStart()
    {
        // mark this game as started
        started = true;

        // hide the game logo
        FadeCanvas(mainLogo);

        // stop blinking the "Tap to Start" text and hide it
        StopCoroutine(blinking);
        blinking = null;
        startText.alpha = 0;

        // set the camera controller to ready so it can set position
        // behind the player
        FindObjectOfType<Broom>().cameraController.SetBool("ready", true);

        // player hasn't read the tutorials
        if (!tutorials)
        {
            // opens the tutorial dialog
            dialogs.Open(0, delegate 
            {
                // and gets the player ready after dialog closes
                dialogs.Open(1);
                GetReady();
            });
        }

        // otherwise, starts the game immediately
        else
        {
            // by getting the player ready on both modes
            Player.ready = true;
            Player.readySide = true;

            // and showing HUD
            FadeCanvas(HUD);
        }
    }

    // Game's Over
    public void GameOver ()
    {
        // mark the game as finished
        gameOver = true;

        // fade the music if not muted
        if (!Mute.mute) audio.Mute(5);

        // updates the coins text on ending screen
        coinsRecordTxt.text = Coins.ToString();

        // if player's beaten his own record, show a "New Record" text and blink it
        if (Coins > coinsRecord) blinking = StartCoroutine(BlinkCanvas(recordText));

        // save the coins amount on PlayerPrefs
        PlayerPrefs.SetInt("Coins", Coins);

        // hide the HUD
        FadeCanvas(HUD);

        // show the ending screen
        FadeCanvas(endScreen, .4f);

        // wait for a while, or the game can reload fast on a accidentally tap
        StartCoroutine(WaitToReload());
    }

    // wait a while on the ending screen
    IEnumerator WaitToReload ()
    {
        yield return new WaitForSeconds(3);

        // then mark game as ready for reloading
        readyToReload = true;
    }

    /// <summary>
    /// Fades a Canvas Group of a given Game Object over a given speed.
    /// Default speed is 1;
    /// </summary>
    /// <param name="canvas">Canvas.</param>
    /// <param name="speed">Speed.</param>
    public void FadeCanvas(GameObject canvas, float speed = 1.0f)
    {
        StartCoroutine(FadeCanvasRoutine(canvas.GetComponent<CanvasGroup>(), speed));
    }

    /// <summary>
    /// Fades the Canvas Group over a given speed.
    /// Default speed is 1;
    /// </summary>
    /// <param name="canvas">Canvas.</param>
    /// <param name="speed">Speed.</param>
    public void FadeCanvas (CanvasGroup canvas, float speed = 1.0f)
    {
        StartCoroutine(FadeCanvasRoutine(canvas, speed));
    }

    /// Fades the Canvas Group over a given speed.
    /// Default speed is 1;
    IEnumerator FadeCanvasRoutine (CanvasGroup canvas, float speed = 1.0f)
    {
        // fade out
        if (canvas.alpha < 0.1f)
        {
            while (canvas.alpha < 1.0f)
            {
                canvas.alpha += speed * Time.deltaTime;
                yield return null;
            }

            canvas.alpha = 1.0f;
        }

        // fade in
        else
        {
            while (canvas.alpha > 0)
            {
                canvas.alpha -= speed * Time.deltaTime;
                yield return null;
            }

            canvas.alpha = 0;
        }
    }

    // Blinks a CanvasGroup in loop
    IEnumerator BlinkCanvas (CanvasGroup canvas)
    {
        yield return FadeCanvasRoutine(canvas);
        canvas.alpha = 1;
        blinking = StartCoroutine(BlinkCanvas(canvas));
    }

    /// <summary>
    /// Gets Player ready, allowing enemies and obstacles to spawn.
    /// </summary>
    public void GetReady ()
    {
        // wait a while so enemies won't show up suddenly
        StartCoroutine(GetReadyRoutine());
    }

    // Gets Player Ready after a while
    IEnumerator GetReadyRoutine ()
    {
        yield return new WaitForSeconds(.5f);
        if (Player.sideScrolling) 
        {
            Player.readySide = true;
            Player.ready = true;
        }

        else 
        {
            Player.ready = true; 
        }
    }

    // Reload the Game
    void Reload ()
    {
        // reset static variables
        Coins = 0;
        readyToReload = false;
        started = false;

        // updates coin text
        coinsTxt.text = "0";

        // reload the sene
        UnityEngine.SceneManagement.SceneManager.LoadScene("MagicRun");

        // hide ending screen
        FadeCanvas(endScreen);

        // shows the game logo once again
        mainLogo.alpha = 1;

        // if something is blinking, stop it
        if (blinking != null)
        {
            StopCoroutine(blinking);
            blinking = null;
        }

        // and blinks the "Tap to Start" text again
        blinking = StartCoroutine(BlinkCanvas(startText));

        // let the music play
        audio.music.Play();
        if (!Mute.mute)  audio.Unmute(0);

        StartCoroutine(ResetGame());
    }

    // avoid reload and start game too fast
    IEnumerator ResetGame ()
    {
        yield return new WaitForSeconds(2);
        gameOver = false;
    }

    // Add one coin to Player
    public void AddCoin ()
    {
        // only if the game isn's over
        if (gameOver) return;

        // add a coin
        Coins++;

        // update coins text
        coinsTxt.text = Coins.ToString();

        // play a sound effect
        if (!Mute.mute) audio.coinSound.Play();
    }
}