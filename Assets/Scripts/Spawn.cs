﻿// Written by Sylker Teles, 2018
using UnityEngine;

/// <summary>
/// Spawns enemies, obstacles and coins prefabs.
/// </summary>
public class Spawn : MonoBehaviour 
{
    // points where spawn is possible
    public Transform[] spawnPoints; 

    // prefabs to be instantiated
    public GameObject[] spawnables;

    // coins are special spawns
    public GameObject coins;

	// Use this for initialization
	void OnEnable () 
    {
        // if player isn't ready, spawns nothing
        if (tag == "3D" && !Player.ready) return;
        if (tag == "2D" && !Player.readySide) return;

        SpawnObject();
	}

    void SpawnObject ()
    {
        // sort a place to put enemies and obstacles
        int spawnPoint = Random.Range(0, spawnPoints.Length);

        // sort a place to put enemies or obstacles
        int coinsPoint = Random.Range(0, spawnPoints.Length);

        // limit the available objects for a easy start 
        int limit = spawnables.Length - 1;

        // after a while unleash the last spawn object
        if (FindObjectOfType<Replicator>().steps > 40) limit += 1;

        // sort the object to be instantiated
        int spawn = Random.Range(0, limit);

        // instantiate the enemy or obstacle
        GameObject newSpawn = Instantiate(
            spawnables[spawn], 
            spawnPoints[spawnPoint].position, 
            spawnPoints[spawnPoint].rotation);

        // instantiate coins
        GameObject newCoins = Instantiate(
            coins, 
            spawnPoints[coinsPoint].position,
            spawnPoints[coinsPoint].rotation);

        // set them as children of this Transform
        newSpawn.transform.SetParent(transform);
        newCoins.transform.SetParent(transform);
    }
}