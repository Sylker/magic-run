﻿// Written by Sylker Teles, 2018

using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Replicates instances of a given modular Game Object.
/// This class will create the whole game level procedurally.
/// </summary>
public class Replicator : MonoBehaviour 
{
    // the prefabs or GameObjects to be replicated
    public GameObject corridorModule, corridorCorner, sideCorridor, sideCorner;

    // current module we are using
    GameObject module;

    // max number of instances to be replicated or how far it should go
    int maxInstances = 10;

    // during side scrolling mode, blocks will change to 2D designed assets
    bool sideScrolling;

    // let's keep tracking of all modules by storing them in a list
    List<GameObject> modules = new List<GameObject>();

    // keep trackin of the total amount of blocks created
    public int steps { get; private set; }

    // and the amount of blocks before changing between 2D and 3D
    int stepsCount;

    // Initialization
    void Start()
    {
        // create the first set of modules
        for (int i = 0; i < maxInstances; i++) CreateModule();
	}
	
    /// <summary>
    /// Creates a module block.
    /// </summary>
    public void CreateModule ()
    {
        // avoid function overlapping
        // replicating = true;

        // define our next module
        module = NextModule();

        // and instantiate it
        GameObject newModule = Instantiate(module, transform);

        // put the module in precise place just after the pevious one
        Vector3 newPosition = module.transform.position;

        if (modules.Count > 0)
        {
            newPosition = modules[modules.Count - 1].transform.position;

            // size of module will determine the distance between them
            float size = 8;

            // in 3D mode, we move towards the Z axis
            if (!sideScrolling) newPosition.z += size;

            // in 2D mode we move toweards the X axis
            else newPosition.x += size;
        }

        // reposition the module
        newModule.transform.position = newPosition;

        // give it a nice name
        // newModule.name = newModule.name + steps;

        // store the module for later use
        modules.Add(newModule);

        // activate it
        newModule.SetActive(true);

        // destroy and remove extra blocks
        if (modules.Count > maxInstances+4)
        {
            // destroy first created module
            Destroy(modules[0]);

            // and clear it from list
            modules.Remove(modules[0]);
        }

        // increase steps counting if player is ready and game isn't over
        if (Player.ready && !Interface.instance.gameOver)
        {
            stepsCount++;
            steps++;
        }
    }

    // returns the next GameObject to be instantiated
    // depending on if we are in 3D or 2D mode or in a corner
    GameObject NextModule ()
    {
        // this is our first module, return a 3D corridor
        if (module == null)
            return Instantiate(corridorModule, transform);

        // we are in a corner to 2D
        if (module == corridorCorner)
        {
            // starts the 2D phase
            sideScrolling = true;

            // return a side scrolling module
            return sideCorridor;
        }

        // we are in a corner to 3D
        else if (module == sideCorner)
        {
            // ends the 2D phase
            sideScrolling = false;

            // returns a 3D module
            return corridorModule;
        }

        // in the first time in 2D mode, lets make it longer
        // so player can learn the movements
        if (!Player.readySide && sideScrolling) maxInstances += 10;
        else if (maxInstances > 10) maxInstances -= 10;

        // not in a corner, check keep counting.
        if (stepsCount > maxInstances)
        {
            // reset the counting
            stepsCount = 0;

            // current mode is 3D, create a corner to 2D
            if (!sideScrolling) return corridorCorner;

            // otherwise, create a corner to 3D
            else return sideCorner;
        }

        // or just return a regular straight module 3D or 2D
        return module;
    }
}